﻿using UnityEngine;
using System.Collections;

public class NavigationScript : MonoBehaviour
{

    bool ableToMove;
    public Transform target;
    NavMeshAgent agent;
    GameObject indicator;
    
    void Start()
    {
        indicator = this.transform.FindChild("Indicator").gameObject;
        agent = GetComponent<NavMeshAgent>();
        ableToMove = false;
    }

    void Update()
    {
        //target = GameObject.Find("Target(Clone)");
        if (ableToMove == true)
        {
            agent.SetDestination(target.transform.position);
        }
        else if (ableToMove == false)
        {
            agent.Stop();
        }
        
    }
    void OnMouseDown()
    {
        if (ableToMove == false)
        {
            ableToMove = true;
            indicator.SetActive(true);
        }
        else if(ableToMove == true)
        {
            ableToMove = false;
            indicator.SetActive(false);
        }
    }


}